import React from "react";
import { Spin } from "antd";

export default function Spinner() {
    return (
        <div>
            <Spin size="large" />
        </div>
    );
}
