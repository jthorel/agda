export default {
    primary: {
        fontFamily: '"Sanchez", serif',
        fontWeight: 'normal'
    },
    semibold: {
        fontFamily: '"Sanchez-semibold", serif',
        fontWeight: 'normal'
    },
    bold: {
        fontFamily: '"Sanchez-bold", serif',
        fontWeight: 'normal'
    },
    italic: {
        fontFamily: '"Sanchez-italic", serif',
        fontWeight: 'normal'
    }
}
