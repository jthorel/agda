export default {
    transparent: "transparent",
    black: "#000000",
    white: "#ffffff",
    link: "#007BCF",
    linkHover: "#05BAFF",

    primary1: "#0A4682",
    primary2: "#009CE0",
    primary3: "#7DC8F0",
    primary4: "#C8E6FA",
    primary5: "#E2F2FC",

    secondary1: "#1E6E32",
    secondary2: "#B0CB0B",
    secondary3: "#D2E182",
    secondary4: "#EBF0BE",
    secondary5: "#F5F7DE",

    tertiary1: "#8C005A",
    tertiary2: "#E6007E",
    tertiary3: "#F0A5C3",
    tertiary4: "#FAD2E6",
    tertiary5: "#FCE8F2",

    quanternary1: "#994916",
    quanternary2: "#FFB900",
    quanternary3: "#FFE57E",
    quanternary4: "#FEF5B2",
    quanternary5: "#FEFAD8",

    quinary1: "#E1DFDA",
    quinary2: "#EEEAE1",
    quinary3: "#F8F6F0",

    senary1: "#363636",
    senary2: "#767676",
    senary3: "#CECECE",
    senary4: "#eeeeee"
};
