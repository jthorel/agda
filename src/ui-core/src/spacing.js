export default {
    marginSM: '19px',
    marginMD: '24px',
    marginLG: '40px',
    gutter: '20px',
    gutterSM: '18px'
}
