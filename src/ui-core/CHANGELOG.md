# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).


## 0.0.5 - 2018-10-17

### Added
- linkHover color
## 0.0.4 - 2018-10-02

### Changed

- colors according to figma design system
